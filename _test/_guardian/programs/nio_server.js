var nio = require("nio");
var crypto = require("crypto-utils");

function onMessage(eventName, params) {
    console.log(eventName, params);
    if (eventName === "file_upload" && params.length === 2) {
        console.log("file: ", params[0]);
        console.log("data: ", crypto.decodeBase64ToText(params[1]));
    }
}

console.log("STARTING...");
var server = nio.newServer(10001);
try {
    server.open();
    server.listen("*", onMessage);

    // mark server is ready
    _global.Set("server", true);

    // lock and wait until close
    server.join();
} catch (err) {
    server.close();
    console.error(err);
}
