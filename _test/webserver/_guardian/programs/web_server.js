var http = require("http");

console.log("STARTING WEB SERVER...");
var app = http.newServer();
app.static({
    "enabled": true,
    "prefix": "/",
    "root": "./www",
    "index": "",
    "compress": true
});
try {
    app.listen([
        {
            "addr": "80",
            "tls": false
        }
    ]);

    // -----------------------
    console.log("JOIN WEB SERVER");
    app.join();
    // -----------------------

} catch (err) {
    app.close();
    console.error(err);
}
