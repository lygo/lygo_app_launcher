package app

import (
	"bitbucket.org/lygo/lygo_app_launcher/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_email"
	"bitbucket.org/lygo/lygo_events"
	"bitbucket.org/lygo/lygo_ext_sms"
	"bitbucket.org/lygo/lygo_updater/src"
	"fmt"
	"os"
	"path/filepath"
	"sync"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Launcher struct {
	root       string
	dirStart   string
	dirApp     string
	dirWork    string
	stopDirs   []string
	settings   *commons.Settings
	logger     *commons.Logger
	updater    *lygo_updater.Updater
	stopTicker *lygo_events.EventTicker
	chanQuit   chan bool
	mux        sync.Mutex
	fileHashes map[string]string
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewAppLauncher(dirStart, dirApp, dirWork, mode string) (*Launcher, error) {
	dirWork = lygo_paths.Absolute(dirWork)
	if len(dirStart) == 0 {
		dirStart = lygo_paths.Dir(dirWork)
	}
	if len(dirApp) == 0 {
		dirApp = lygo_paths.Dir(dirWork)
	}
	lygo_paths.GetWorkspace(commons.DirStart).SetPath(dirStart)
	lygo_paths.GetWorkspace(commons.DirApp).SetPath(dirApp)
	lygo_paths.SetWorkspacePath(dirWork)
	err := lygo_paths.Mkdir(lygo_paths.GetWorkspacePath() + string(os.PathSeparator))
	if nil != err {
		return nil, err
	}

	instance := new(Launcher)
	instance.fileHashes = make(map[string]string)
	instance.chanQuit = make(chan bool, 1)
	instance.dirStart = lygo_paths.GetWorkspace(commons.DirStart).GetPath()
	instance.dirApp = lygo_paths.GetWorkspace(commons.DirApp).GetPath()
	instance.dirWork = lygo_paths.GetWorkspacePath()
	instance.root = filepath.Dir(instance.dirWork)
	instance.stopDirs = []string{
		instance.root,
		instance.dirWork,
		instance.dirApp,
	}
	instance.logger = commons.NewLogger(commons.ModeProduction)
	if mode == commons.ModeDebug {
		instance.logger.SetLevel(commons.ModeDebug)
	}

	err = instance.init()

	if nil == err {
		instance.logger.Debug(fmt.Sprintf("running with settings: %v", instance.settings.String()))
	}

	return instance, err
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Launcher) GetLogger() *commons.Logger {
	return instance.logger
}

func (instance *Launcher) IsRunning() bool {
	if nil != instance && nil != instance.stopTicker && nil != instance.updater {
		return instance.updater.IsProcessRunning()
	}
	return false
}

func (instance *Launcher) Restart() {
	if nil != instance && instance.IsRunning() {
		instance.updater.ReStart()
	}
}

func (instance *Launcher) Start() error {
	if nil != instance.updater {
		if nil != instance.stopTicker {
			instance.stopTicker.Start()
		}

		_, _, _, _, err := instance.updater.Start()

		return err
	}
	return commons.UpdaterIsNotReadyError
}

func (instance *Launcher) Wait() error {
	if nil != instance.updater {
		<-instance.chanQuit // wait exit
		instance.chanQuit = make(chan bool, 1)
	}
	return commons.UpdaterIsNotReadyError
}

func (instance *Launcher) Stop() {
	if nil != instance {
		if nil != instance.updater {
			instance.updater.Stop()
		}
		if nil != instance.stopTicker {
			instance.stopTicker.Stop()
		}

		cmd := instance.settings.StopCmd
		if len(cmd) > 0 {
			// file check
			if b, _ := lygo_paths.Exists(cmd); b {
				_ = lygo_io.Remove(cmd)
			}
		}

		instance.logger.Info("Stopped!")

		instance.chanQuit <- true
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Launcher) init() error {

	// detect configuration file
	config, err := instance.config()
	if nil != err {
		return err
	}

	instance.settings = config
	instance.updater = lygo_updater.NewUpdater(config.String())
	instance.updater.SetRoot(instance.root)
	instance.updater.OnError(instance.onUpdaterError)
	instance.updater.OnUpgrade(instance.onUpdaterUpgrade)
	instance.updater.OnLaunchStart(instance.onLaunchStart)
	instance.updater.OnLaunchQuit(instance.onLaunchQuit)
	instance.updater.OnTask(instance.onTask)

	instance.stopTicker = lygo_events.NewEventTicker(3*time.Second, func(t *lygo_events.EventTicker) {
		instance.checkStop()
		instance.logger.Debug("Checking for stop command....")
	})

	return nil
}

func (instance *Launcher) config() (*commons.Settings, error) {
	var config *commons.Settings
	files, err := lygo_paths.ListFiles(instance.dirWork, "*.json")
	if nil != err {
		return nil, err
	}

	for _, file := range files {
		txt, err := lygo_io.ReadTextFromFile(file)
		if nil == err && len(txt) > 0 {
			config, err = commons.NewSettings(txt)
			if nil == err {
				if len(config.CommandToRun) > 0 {
					// found a valid configuration file
					return config, nil
				}
			}
		}
	}

	return nil, commons.InvalidConfigurationError // not found
}

func (instance *Launcher) onUpdaterError(err string) {
	if nil != instance.logger {
		instance.logger.Error(fmt.Sprintf("Updater Error: %v", err))
		instance.notify("error", fmt.Sprintf("ERROR: %s", err))
	}
}

func (instance *Launcher) onUpdaterUpgrade(fromVersion string, toVersion string, files []string) {
	if nil != instance.logger {
		instance.logger.Info(fmt.Sprintf("Updated from '%s' to '%s'.", fromVersion, toVersion))
		instance.notify("update", fmt.Sprintf("UPDATE: from version '%s' to version '%s' files=%v", fromVersion, toVersion, len(files)))
	}
}

func (instance *Launcher) onLaunchStart(command string) {
	if nil != instance.logger {
		instance.logger.Info(fmt.Sprintf("Started exec '%s'.", command))
		instance.notify("start", fmt.Sprintf("START: %s", command))
	}
}

func (instance *Launcher) onLaunchQuit(command string, pid int) {
	if nil != instance.logger {
		instance.logger.Info(fmt.Sprintf("Quitting exec '%s' pid %v.", command, pid))
		instance.notify("quit", fmt.Sprintf("QUIT: %s", command))
	}
}

func (instance *Launcher) onTask(uid string, payload map[string]interface{}) {
	if nil != instance && nil != payload {
		event := lygo_reflect.GetString(payload, "event")
		switch event {
		case commons.TaskEventFileChanged:
			filename := lygo_reflect.GetString(payload, "file")
			if instance.isFileChanged(filename) {
				action := lygo_reflect.GetString(payload, "action")
				switch action {
				case commons.TaskActionRestart:
					instance.Restart()
					instance.notify("task", fmt.Sprintf("TASK '%s' EXECUTED: %s", uid, lygo_conv.ToString(payload)))
				default:
					instance.Restart()
					instance.notify("task", fmt.Sprintf("TASK '%s' EXECUTED: %s", uid, lygo_conv.ToString(payload)))
				}
			}
		default:
			// not supported
		}
	}
}

func (instance *Launcher) checkStop() {
	if nil != instance && nil != instance.settings {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		cmd := instance.settings.StopCmd

		// check if file exists
		for _, path := range instance.stopDirs {
			cmdFile := lygo_paths.Concat(path, cmd)
			if b, _ := lygo_paths.Exists(cmdFile); b {
				_ = lygo_io.Remove(cmdFile)
				instance.Stop()
				return
			}
		}
	}
}

func (instance *Launcher) isFileChanged(filename string) bool {
	abs := lygo_paths.WorkspacePath(filename)
	hash, err := lygo_io.ReadHashFromFile(abs)
	if nil == err {
		if value, b := instance.fileHashes[abs]; b {
			instance.fileHashes[abs] = hash
			return value != hash
		} else {
			// just add file first time
			instance.fileHashes[abs] = hash
		}
	}
	return false
}

func (instance *Launcher) notify(event, message string) {
	if nil != instance && nil != instance.settings && nil != instance.settings.Notifications && len(message) > 0 {
		// SMS
		if nil != instance.settings.Notifications.Sms && len(instance.settings.Notifications.Sms) > 0 {
			to := lygo_reflect.GetString(instance.settings.Notifications.Sms, "to")
			from := lygo_reflect.GetString(instance.settings.Notifications.Sms, "from")
			if len(from) == 0 {
				from = "App Launcher"
			}
			if len(to) > 0 {
				sms := lygo_ext_sms.NewSMSEngine(instance.settings.Notifications.Sms)
				if len(sms.ProviderNames()) > 0 {
					// READY TO SEND
					_, _ = sms.SendMessage("", message, to, from)
				}
			}
		}
		// EMAIL
		if nil != instance.settings.Notifications.Email && len(instance.settings.Notifications.Email) > 0 {
			to := lygo_reflect.GetString(instance.settings.Notifications.Email, "to")
			from := lygo_reflect.GetString(instance.settings.Notifications.Email, "from")
			if len(to) > 0 {
				smtp, err := lygo_email.NewSender(instance.settings.Notifications.Email)
				if nil == err {
					subject := fmt.Sprintf("%s (%s) - %s", commons.Name, commons.Version, event)
					_ = smtp.Send(subject, message, []string{to}, from, nil)
				}
			}
		}
	}
}
