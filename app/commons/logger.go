package commons

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_logs"
	"fmt"
	"os"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Logger struct {
	mode string
	root string
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewLogger(mode string) *Logger {
	instance := new(Logger)
	instance.mode = mode
	instance.root = lygo_paths.WorkspacePath("./logging/")
	_ = lygo_paths.Mkdir(instance.root + string(os.PathSeparator))

	if mode == ModeDebug {
		lygo_ext_logs.SetLevel(lygo_ext_logs.LEVEL_DEBUG)
	} else {
		lygo_ext_logs.SetLevel(lygo_ext_logs.LEVEL_INFO)
	}
	lygo_ext_logs.SetOutput(lygo_ext_logs.OUTPUT_FILE)

	// reset file
	_, _ = lygo_io.WriteTextToFile("", lygo_paths.Concat(instance.root, "logging.log"))

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Logger) Close() {
	lygo_ext_logs.Close()
}

func (instance *Logger) SetLevel(level string) {
	lygo_ext_logs.SetLevelName(level)
}

func (instance *Logger) GetLevel() int {
	return lygo_ext_logs.GetLevel()
}

func (instance *Logger) Debug(args ...interface{}) {
	// file logging
	lygo_ext_logs.Debug(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}

func (instance *Logger) Info(args ...interface{}) {
	// file logging
	lygo_ext_logs.Info(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}

func (instance *Logger) Error(args ...interface{}) {
	// file logging
	lygo_ext_logs.Error(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}

func (instance *Logger) Warn(args ...interface{}) {
	// file logging
	lygo_ext_logs.Warn(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}
