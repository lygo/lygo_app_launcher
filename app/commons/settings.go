package commons

import (
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_updater/src"
	_ "embed"
	"errors"
	"fmt"
)

//go:embed settings.json
var TplSettings string

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type NotificationSettings struct {
	Email map[string]interface{} `json:"email"`
	Sms   map[string]interface{} `json:"sms"`
}

type Settings struct {
	StopCmd       string                `json:"stop_cmd"`
	Notifications *NotificationSettings `json:"notifications"`
	lygo_updater.Settings

	dirWork string
}

// ---------------------------------------------------------------------------------------------------------------------
//		c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func DiscoverSettings() (*Settings, error) {
	instance := new(Settings)
	instance.dirWork = lygo_paths.GetWorkspacePath()
	err := instance.init()
	if nil != err {
		return nil, err
	}
	return instance, nil
}

func NewSettings(text string) (*Settings, error) {
	instance := new(Settings)
	err := instance.Parse(text)

	return instance, err
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Settings) Parse(text string) error {
	return lygo_json.Read(text, &instance)
}

func (instance *Settings) String() string {
	return lygo_json.Stringify(instance)
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Settings) init() error {
	files, err := lygo_paths.ListFiles(instance.dirWork, "*.json")
	if nil != err {
		return err
	}

	// search a valid file
	for _, file := range files {
		txt, err := lygo_io.ReadTextFromFile(file)
		if nil == err && len(txt) > 0 {
			err = instance.Parse(txt)
			if nil == err {
				if len(instance.CommandToRun) > 0 {
					if instance.VersionFile == "REMOTE-FILE-WITH-VERSION" {
						return lygo_errors.Prefix(errors.New(fmt.Sprintf("This file '%v' is the auto-generated file. Please, edit with your data.", file)), InvalidConfigurationError.Error()+":")
					}
					// found a valid configuration file
					return nil
				}
			}
		}
	}

	// creates new config file from template
	filename := lygo_paths.Concat(instance.dirWork, "settings.json")
	_, err = lygo_io.WriteTextToFile(TplSettings, filename)
	if nil != err {
		return err
	}

	return lygo_errors.Prefix(errors.New(fmt.Sprintf("Created new configuration file into '%v'", filename)), MissingConfigurationError.Error()+":")
}
