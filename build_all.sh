##!/bin/sh

echo "START BUILDING..."
sh ./build_mac.sh
sh ./build_linux.sh
sh ./build_linux_raspberry_3.sh
sh ./build_windows.sh
echo "END BUILDING."