package main

import (
	"bitbucket.org/lygo/lygo_app_launcher/app"
	"bitbucket.org/lygo/lygo_app_launcher/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"flag"
	"fmt"
	"path/filepath"
	"time"
)

var logger *commons.Logger

//----------------------------------------------------------------------------------------------------------------------
//	l a u n c h e r
//----------------------------------------------------------------------------------------------------------------------

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] APPLICATION %s ERROR: %s. Workspace: %s",
				commons.Name, r, lygo_paths.GetWorkspacePath())
			if nil != logger {
				logger.Error(message)
				time.Sleep(3 * time.Second) // wait logger can write
			} else {
				fmt.Println(message)
			}
		}
	}()

	// flags
	dirStart := flag.String("dir_start", "", "Set a particular folder as launch directory")
	dirApp := flag.String("dir_app", "", "Set a particular folder as app directory")
	dirWork := flag.String("dir_work", "./_update_workspace", "Set a particular folder as main workspace")
	mode := flag.String("m", commons.ModeProduction, "Mode allowed: 'debug' or 'production'")
	flag.Parse()

	program, err := app.NewAppLauncher(*dirStart, *dirApp, *dirWork, *mode)

	if nil == err {
		// get logger from guardian
		logger = program.GetLogger()
		logger.Info(lygo_strings.Format("APPLICATION %s v.%s", commons.Name, commons.Version))
		logger.Info(lygo_strings.Format("DIR_START: %s", lygo_paths.GetWorkspace(commons.DirStart).GetPath()))
		logger.Info(lygo_strings.Format("DIR_APP: %s", lygo_paths.GetWorkspace(commons.DirApp).GetPath()))
		logger.Info(lygo_strings.Format("DIR_WORK: %s", lygo_paths.GetWorkspacePath()))
		logger.Info(lygo_strings.Format("DIR_ROOT: %s", filepath.Dir(lygo_paths.GetWorkspacePath())))
		err = program.Start()
		if nil != err {
			panic(err)
		}

		_ = program.Wait()
	} else {
		panic(err)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
