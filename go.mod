module bitbucket.org/lygo/lygo_app_launcher

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.104
	bitbucket.org/lygo/lygo_email v0.1.5
	bitbucket.org/lygo/lygo_events v0.1.9
	bitbucket.org/lygo/lygo_ext_logs v0.1.6
	bitbucket.org/lygo/lygo_ext_sms v0.1.5
	bitbucket.org/lygo/lygo_updater v0.1.29
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
)
